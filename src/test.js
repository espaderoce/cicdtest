const supertest = require('supertest');
const server = require('./index');
const chai = require('chai');
const should = require('should');
// chai.should();

const api = supertest.agent(server);

describe('Add method', () => {
    it('should connect to the Server', (done) => {
        api.post('/users')
            // .set('Connetion', 'keep alive')
            .set('Content-Type', 'application/json')
            .type('form')
            .send({
                name: "Joaquin Chumacero",
                email: "joaquin@gmail.com"
            })
            .end((err, res) => {
                res.should.have.property('status', 200)
                    // res.status.should.equal(200);
                    // res.body.result.should.equal(5);
                    // should.exist(res.body);
                    // should(res.body).be.a('array');
                    // res.should(res.body).be.a('array');
                done();
            });
    });
})