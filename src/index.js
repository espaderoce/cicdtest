const express = require("express");
const app = express();
const bodyParser = require('body-parser');

// Destructure our bodyParser methods
const { urlencoded, json } = bodyParser;

app.use(json());
app.use(urlencoded({ extended: false }));
//setting
app.set('port', process.env.PORT || 3000); //si nos asignan unpuerto que lo tome, si no que use el 3000
//app.set('json spaces',2);


//middlewares
// app.use(express.json()); //para que nuestro servidor reciba dato de tipo json
// app.use(
//     express.urlencoded({
//         extended: false,
//     })
// ); //para que puea recibir datos desde formularios/ con el extended false decimos que no recibiremos imagenes solo texto o numeros

//routes

app.use(require("./route/index"));
// app.get('/', (req, res) => {

//     // res.send('Hello, welcome to my project');
//     // res.write('Hello world');
//     res.json({
//         'hello': 'hola'
//     });

// });




app.listen(app.get('port'), () => {

    console.log("Server on port 3000");
});

module.exports = app;