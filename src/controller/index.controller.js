const {
    Pool
} = require('pg');


const pool = new Pool({
    host: 'localhost',
    user: 'postgres',
    password: 'database',
    database: 'practica',
    port: '5432'
});

const getUsers = async(req, res) => {

    const response = await pool.query('SELECT * FROM users');
    console.log(response.rows);
    res.status(200).json(response.rows);
}

const getUserbyId = async(req, res) => {
    var id = req.params.id;
    const response = await pool.query('SELECT * FROM users WHERE id=$1', [id]);
    //   console.log(response);
    res.json(response.rows);
}

const delUserbyId = async(req, res) => {
    const id = req.params.id;
    const response = await pool.query('DELETE FROM users WHERE id=$1', [id]);
    console.log(response);
    res.json(`User ${id} deletd successfully`);

}

const updateUser = async(req, res) => {
    var id = req.params.id;
    const {
        name,
        email
    } = req.body;
    const response = await pool.query('UPDATE users SET name=$1,email=$2 WHERE id=$3', [name, email, id])
    res.json('User Updated Successfully');
    console.log(response);
}


const createUser = (req, res) => {
    const {
        name,
        email
    } = req.body;
    const response = pool.query('INSERT INTO users (name, email) VALUES ($1,$2)', [name, email]);
    console.log(response);
    res.json({
        message: "User addedSuccessfully",
        body: {
            user: {
                name,
                email
            }
        }
    })

};


const welcome = (req, res) => {

    res.status(200).send({ hello: 'Welcome to my project using gitlab runner and docker 2021, It is running on docker container' });
}


module.exports = {
    welcome,
    getUsers,
    createUser,
    getUserbyId,
    delUserbyId,
    updateUser
};