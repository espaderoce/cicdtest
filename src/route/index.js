const { Router } = require('express');
const router = Router();
const {
    welcome,
    getUsers,
    createUser,
    getUserbyId,
    delUserbyId,
    updateUser
} = require('../controller/index.controller');

router.get('/', welcome);
router.get('/users', getUsers);
router.get('/users/:id', getUserbyId);
router.post('/users', createUser);
router.delete('/users/:id', delUserbyId);
router.put('/users/:id', updateUser);


module.exports = router;